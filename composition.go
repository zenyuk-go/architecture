package main

type Plugin interface {
	Init()
	Shutdown()
	Handle()
}

type DefaultPlugin struct {}

func (p DefaultPlugin) Init() {}
func (p DefaultPlugin) Shutdown() {}
func (p DefaultPlugin) Handle() {}

// Foo only runs init()
type FooPlugin struct {
	DefaultPlugin
}

func (p FooPlugin) Init() {
	println("foo!")
}

// run it
func main() {
	var p Plugin = &FooPlugin{}
	p.Init()
	p.Shutdown()
}
